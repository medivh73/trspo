// lab4.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#include <omp.h>
#include <iostream>
#include <Windows.h>
#include <ctime>
int main(int argc, char* argv[])
{
	int  Arr[10][10], B[10];
	for (int i = 0; i < 10; ++i) {
		for (int j = 0; j < 10; ++j) {
			Arr[i][j] = rand() % 10;
			if(j==9) std::cout << Arr[i][9] << std::endl;
			else
			std::cout << Arr[i][j] << " ";
		}
	}
	for (int i = 0; i < 10; ++i) B[i] = 0;
	unsigned int start_time = clock(); // ��������� �����
#pragma omp parallel 
	{
		int n = 0;
#pragma omp for
		for (int i = 0; i < 10; ++i) {
			int sum = 0;
			for (int j = 0; j < 10; ++j)
			{
				n = omp_get_thread_num();
				if (i & 1)  sum += Arr[i][j]; 

			}
			B[i] = sum;
			//std::cout << i << " " << sum << std::endl;
		}
	}
	for (int i = 0; i < 10; ++i) std::cout << B[i] << std::endl;
	unsigned int end_time = clock(); // �������� �����
	unsigned int search_time = end_time - start_time; // ������� �����
	std::cout << "Time= " << search_time << " ms" << std::endl;
	system("PAUSE");
	return 0;
}