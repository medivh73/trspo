// lab33.cpp: ���������� ����� ����� ��� ����������� ����������.
//


#include "stdafx.h"
#include <omp.h>
#include <iostream>
#include <Windows.h>
int main(int argc, char* argv[])
{
	omp_set_num_threads(2);
#pragma omp parallel
	{
		std::cout << "Parallel region 1\n" << std::endl;
	}
#pragma omp parallel
	{
		std::cout << "Parallel region 2\n" << std::endl;
	}
	system("PAUSE");
	return 0;
}